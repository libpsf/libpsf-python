#!/bin/bash
set -e -x -u

#############################################################################
#############################################################################
###
###
###         REQUIRES:  $PYDIR
###         THIS WILL BUILD A WHEEL FOR PYTHON SYSTEM FOUND AT /opt/python/$PYDIR/
###
###
#############################################################################
#############################################################################

PYBIN="/opt/python/${PYDIR}/bin"

# in wheel root:
###########################################
cd /io/libpsf
cp /io/README.md .

# update tools and install reqs
"${PYBIN}/pip" install --upgrade pip
"${PYBIN}/pip" install -r /io/dev-requirements.txt

# start building
"${PYBIN}/python" setup.py build_ext
"${PYBIN}/pip" wheel --no-deps -w /io/dist/ .

# use latest python 3 to audit the wheel -- but do not audit the 2.7mu wheel
if [[ ! $PYBIN =~ *cp27mu* ]]; then
    echo "*****************installing auditwheel**********************"
    /opt/python/${AUDITWHEEL_PY}/bin/pip install auditwheel
    /opt/python/${AUDITWHEEL_PY}/bin/python -m auditwheel repair /io/dist/libpsf*.whl -w /io/dist/
else
    echo "*****************installing auditwheel (UCS-2) **********************"
    ${PYBIN}/pip install auditwheel
    ${PYBIN}/python -m auditwheel repair /io/dist/libpsf*.whl -w /io/dist/
fi

# Move fixed-up wheel to wheelhouse
mv /io/dist/libpsf*manylinux*.whl /wheelhouse/
echo "WHEELHOUSE CONTENT:"
ls /wheelhouse


# install the wheel and test ###################################################
"${PYBIN}/pip" install -f /wheelhouse --no-index libpsf
#"${PYBIN}/nosetests" libpsf
teststr="$( ${PYBIN}/python -c 'import libpsf; result=True if "PSFDataSet" in dir(libpsf) else False; print(result)' )"
echo "teststr: $teststr"
if test $teststr == "False"; then
    echo "failed test"
    return 1
else
    echo "passed test"
fi


echo "WHEELHOUSE CONTENT:"
ls /wheelhouse



# conditionally upload #########################################################
if [[ $CI_COMMIT_REF_SLUG == $CI_DEFAULT_BRANCH ]]; then
    echo "I'm on the default branch, so try to publish"
    yum install -y curl
    curl https://sh.rustup.rs -sSf | sh -s -- -y
    yum install -y libffi libffi-devel
    /opt/python/${AUDITWHEEL_PY}/bin/pip install twine pyopenssl
    for WHEEL in /wheelhouse/*.whl; do
        /opt/python/${AUDITWHEEL_PY}/bin/python -m twine upload --verbose "$WHEEL"
    done
else
    echo "I'm not on the default branch, don't try to publish"
fi


echo "WHEELHOUSE CONTENT:"
ls /wheelhouse
